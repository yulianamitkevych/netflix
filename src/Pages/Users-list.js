import React from "react";
import '../Styles/Users-list.css';
import LazyLoad from 'react-lazyload';
import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import {Redirect} from "react-router-dom";

const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);

function UserList(props) {
    let [UsersList, setUsersList] = React.useState([]);

    let [SearchPeopleInputValue, setSearchPeopleInputValue] = React.useState([]);

    let [UserName, setUserName] = React.useState([]);
    let [UserLastName, setUserLastName] = React.useState([]);
    let [UserAge, setUserAge] = React.useState([]);
    let [UserGender, setUserGender] = React.useState([]);
    let [UserCity, setUserCity] = React.useState([]);
    let [CurrentUser, setCurrentUser] = React.useState([]);
    let [UsersUID, setUsersUID] = React.useState([]);

    if (!localStorage.getItem('uid')) {
        window.location.replace('/main');
    }

    function CleanLocalStorage() {
        localStorage.removeItem('uid');
    }

    React.useEffect(() => {
        GetPeopleList();
        GetCurrentUserInfo();
        SetUid();
    }, []);

    function SetUid() {
        setUsersUID(UsersUID = localStorage.getItem('uid'));
    }

    function GetCurrentUserInfo() {
        let ref = firebase.database().ref();
        ref.on("value", function (snapshot) {
            let Users = snapshot.val();
            let refPath = 'user_' + localStorage.getItem('uid');
            setCurrentUser(CurrentUser = Users.users[refPath]);
            setUserName(UserName = CurrentUser.name);
            setUserLastName(UserLastName = CurrentUser.last_name);
            setUserAge(UserAge = CurrentUser.age);
            setUserGender(UserGender = CurrentUser.gender);
            setUserCity(UserCity = CurrentUser.city);
        }, function (error) {
            return `Error: ${error.code}`;
        });
    }

    function GetPeopleList() {
        fetch("https://api.tvmaze.com/people")
            .then(res => res.json())
            .then((Users) => {
                setUsersList(Users);
            })
    }

    function SearchingPeople() {
        fetch(`https://api.tvmaze.com/search/people?q=${SearchPeopleInputValue}`)
            .then(res => res.json())
            .then((Response) => {
                    let List = [];
                    Response.map((Person) => {
                        List.push(Person.person);
                    });
                    setUsersList(List);
                },
            );
    }

    function GetSearchPeopleInputValue(Event) {
        setSearchPeopleInputValue(Event.target.value);
    }

    function InFriends(User) {
        if (CurrentUser.friends) {
            let item = CurrentUser?.friends.filter(function (Friend) {
                return Friend?.id === User?.id;
            });
            if (item.length) {
                return true;
            }
        }
        return false;
    }

    function AddFriend(Event) {
        GetCurrentUserInfo();
        let UserId = Event.target.closest('button').getAttribute('data-user-id');
        let NewFriend = GetUser(UserId)[0];
        let refPath = 'users/user_' + UsersUID + '/friends';
        if (!CurrentUser?.friends) {
            firebase.database().ref(refPath).set([NewFriend]);
        } else {
            let IsFriend = CurrentUser.friends.filter(function (Show) {
                return parseInt(Show.id) === parseInt(NewFriend.id);
            });
            if (IsFriend.length === 0) {
                CurrentUser.friends.push(NewFriend);
                firebase.database().ref(refPath).set(CurrentUser.friends);
            } else {
                alert('You are already friends');
            }
        }
    }

    function GetUser(UserId) {
        return UsersList.filter(function (User) {
            return parseInt(User.id) === parseInt(UserId);
        })
    }

    return (
        <div>
            <header className="header show-list-header">
                <div className="container header-container">
                    <div className="header-top">
                        <div className="top-icon">MY STREAMING</div>
                        <div className="top-items">
                            <div className="top-search">
                                <label/>
                                <input name="top-search" id="top_search" type="text" placeholder="Search People"
                                       onChange={GetSearchPeopleInputValue}/>
                                <button className="top__search-btn" onClick={SearchingPeople}>
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search"
                                         className="svg-inline--fa fa-search fa-w-16" role="img"
                                         xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                              d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"/>
                                    </svg>
                                </button>
                            </div>
                            <div className="top-user">
                                <div className="user-like">
                                    <a href={'/follows'}>
                                        <svg aria-hidden="true" focusable="false" data-prefix="far"
                                             data-icon="heart"
                                             className="svg-inline--fa fa-heart fa-w-16" role="img"
                                             xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                  d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"/>
                                        </svg>
                                    </a>
                                </div>
                                <div className="user-icon dropdown">
                                    <div className="dropdown-content">
                                        <div className="dropdown-user">
                                            <div className="dropdown__user-photo"/>
                                            <div
                                                className="dropdown__user-name">{`${UserName} ${UserLastName}`}</div>
                                            <div className={'dropdown__user-info'}>
                                                <div className="dropdown__user-age">Age: {UserAge} year(s)</div>
                                                <div className="dropdown__user-gender">Gender: {UserGender}</div>
                                                <div className="dropdown__user-city">City: {UserCity}</div>
                                            </div>
                                        </div>
                                        <div className="dropdown-personal">
                                            <div className="dropdown__personal-follows">
                                                <a href={'/follows'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="far"
                                                         data-icon="heart"
                                                         className="svg-inline--fa fa-heart fa-w-16" role="img"
                                                         xmlns="http://www.w3.org/2000/svg"
                                                         viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                              d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"/>
                                                    </svg>
                                                    Follows
                                                </a>
                                            </div>
                                            <div className="dropdown__personal-friends">
                                                <a href={'/friends'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                         data-icon="user-plus"
                                                         className="svg-inline--fa fa-user-plus fa-w-20" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                              d="M624 208h-64v-64c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v64h-64c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h64v64c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-64h64c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zm-400 48c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"/>
                                                    </svg>
                                                    Friends
                                                </a>
                                            </div>
                                            <div className="dropdown__personal-friends" onClick={CleanLocalStorage}>
                                                <a href={'/main'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                         data-icon="sign-out-alt"
                                                         className="svg-inline--fa fa-sign-out-alt fa-w-16"
                                                         role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                              d="M497 273L329 441c-15 15-41 4.5-41-17v-96H152c-13.3 0-24-10.7-24-24v-96c0-13.3 10.7-24 24-24h136V88c0-21.4 25.9-32 41-17l168 168c9.3 9.4 9.3 24.6 0 34zM192 436v-40c0-6.6-5.4-12-12-12H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h84c6.6 0 12-5.4 12-12V76c0-6.6-5.4-12-12-12H96c-53 0-96 43-96 96v192c0 53 43 96 96 96h84c6.6 0 12-5.4 12-12z"/>
                                                    </svg>
                                                    Sign out
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main className="main">
                <div className="container">
                    <div className="users-list">
                        <div className="list-heading">
                            <div className={'list__heading-users'}>People</div>
                            <div>|</div>
                            <div className={'list__heading-shows'}><a href={'/shows'}>Shows</a></div>
                        </div>
                        <hr className="user-hr"/>
                        <div className="users">
                            {UsersList.map((User) => {
                                let UserPhoto = User.image;
                                let UserCountry = User.country;

                                return <div className="users-user" key={User.id}>
                                    <div className="users__user-content">
                                        <div className="users__user-photo">
                                            {UserPhoto ? (
                                                <LazyLoad className={'users-img-lazy'}><img alt={User.name} src={UserPhoto.medium}/></LazyLoad>
                                            ) : ('')}
                                        </div>
                                        <div className="users__user-info">
                                            {User.name ? (
                                                <div className="users__user__info-name">Name: {User.name}</div>
                                            ) : ('')}
                                            {User.birthday ? (
                                                <div className="users__user__info-date">Date of
                                                    birth: {User.birthday}</div>
                                            ) : ('')}
                                            {User.deathday ? (
                                                <div className="users__user__info-date">Date of
                                                    birth: {User.deathday}</div>
                                            ) : ('')}
                                            {User.gender ? (
                                                <div className="users__user__info-sex">Gender: {User.gender}</div>
                                            ) : ('')}
                                            {UserCountry ? (
                                                <div
                                                    className="users__user__info-country">Country: {UserCountry.name}</div>
                                            ) : ('')}
                                        </div>
                                    </div>
                                    <div className="users__user-btn">
                                        <button className="add-to-friends" data-user-id={User.id}
                                                onClick={AddFriend}>{InFriends(User) ? '✔' : '+'}
                                        </button>
                                    </div>
                                </div>
                            })}
                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}

export default UserList;