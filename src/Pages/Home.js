import React from 'react';
import '../App.css'
import LazyLoad from 'react-lazyload';

function Home() {
    return (
        <div>
            <header className="header">
                <div className="header-bg">
                    <img className="bg-img" alt="header-img"
                         src="https://assets.nflxext.com/ffe/siteui/vlv3/5a27cb25-33a9-4bcc-b441-95fefabcbd37/00b3f8f4-5bea-4451-ad36-89c9bd43db7e/UA-ru-20210823-popsignuptwoweeks-perspective_alpha_website_large.jpg"/>
                </div>
                <div className="header-bg-gradient">
                    <div className="container header-container">
                        <div className="header-top">
                            <div className="top-icon">MY STREAMING</div>
                            <div className="top-btn">
                                <button className="enter-btn"><a href={'/sign_in'}>Sign in</a></button>
                            </div>
                        </div>
                        <div className="header-main">
                            <div className="header__main-heading">Unlimited movies, TV shows, and more.</div>
                            <div className="header__main-text">Watch anywhere. Cancel anytime.</div>
                            <div className="header__main-last">Ready to watch? Enter your email to create or restart
                                your membership.
                            </div>
                            <div className="input-group">
                                <div className="header__main-btn">
                                    <a href={'/registration'}>
                                        <button>Get Started
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                 data-icon="chevron-right"
                                                 className="svg-inline--fa fa-chevron-right fa-w-10" role="img"
                                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                                <path fill="currentColor"
                                                      d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/>
                                            </svg>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main className="main">
                <div className="container">
                    <div className="cards">
                        <div className="card">
                            <div className="card-describe">
                                <div className="card-heading">Enjoy on your TV.</div>
                                <div className="card-text">Watch on Smart TVs, Playstation, Xbox, Chromecast, Apple TV,
                                    Blu-ray players, and more.
                                </div>
                            </div>
                            <div className="card-img">
                                <div className={'card__img-img'}>
                                    <LazyLoad>
                                        <img alt="1"
                                             src="https://assets.nflxext.com/ffe/siteui/acquisition/ourStory/fuji/desktop/tv.png"/>
                                    </LazyLoad>
                                </div>
                                <div className={'card__img-video'}>
                                    <LazyLoad>
                                        <video autoPlay playsInline muted loop>
                                            <source
                                                src={'https://assets.nflxext.com/ffe/siteui/acquisition/ourStory/fuji/desktop/video-tv-0819.m4v'}
                                                type={'video/mp4'}/>
                                        </video>
                                    </LazyLoad>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div className="card odd-card">
                            <div className="card-describe">
                                <div className="card-heading">Download your shows to watch offline.</div>
                                <div className="card-text">Save your favorites easily and always have something to
                                    watch.
                                </div>
                            </div>
                            <div className="card-img">
                                <LazyLoad>
                                    <img alt="2"
                                         src="https://assets.nflxext.com/ffe/siteui/acquisition/ourStory/fuji/desktop/mobile-0819.jpg"/>
                                </LazyLoad>
                            </div>
                        </div>
                        <hr/>
                        <div className="card">
                            <div className="card-describe">
                                <div className="card-heading">Watch everywhere.</div>
                                <div className="card-text">Stream unlimited movies and TV shows on your phone, tablet,
                                    laptop, and TV without paying more.
                                </div>
                            </div>
                            <div className="card-img">
                                <div className={'card__img-img'}>
                                    <LazyLoad>
                                        <img alt="3"
                                             src="https://assets.nflxext.com/ffe/siteui/acquisition/ourStory/fuji/desktop/device-pile.png"/>
                                    </LazyLoad>
                                </div>
                                <div className={'card__img-video third-card-video'}>
                                    <LazyLoad>
                                        <video autoPlay playsInline muted loop className={'third-video'} id={'third'}>
                                            <source
                                                src={'https://assets.nflxext.com/ffe/siteui/acquisition/ourStory/fuji/desktop/video-tv-0819.m4v'}
                                                type={'video/mp4'}/>
                                        </video>
                                    </LazyLoad>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div className="card odd-card">
                            <div className="card-describe">
                                <div className="card-heading">Create profiles for kids.</div>
                                <div className="card-text">Send kids on adventures with their favorite characters in a
                                    space made just for them—free with your membership.
                                </div>
                            </div>
                            <div className="card-img">
                                <LazyLoad>
                                    <img alt="4"
                                         src="https://occ-0-4012-1432.1.nflxso.net/dnm/api/v6/19OhWN2dO19C9txTON9tvTFtefw/AAAABdFTpLmANuJpYneLq8L5m7CunMCi8e8Nl4y7xaPVWzG3IeoDoq17egTQAthApKg_4sdRWdwuR8KadWu1frjL3JQImpwq.png?r=fcd"/>
                                </LazyLoad>
                            </div>
                        </div>
                        <hr/>
                    </div>
                    <div className="ready-to-subscribe">
                        <div className="subscribe-heading">Ready to watch? Enter your email to create or restart your
                            membership.
                        </div>
                        <div className="input-group">
                            <div className="header__main-btn">
                                <a href={'/registration'}>
                                    <button>Get Started
                                        <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                             data-icon="chevron-right"
                                             className="svg-inline--fa fa-chevron-right fa-w-10" role="img"
                                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                            <path fill="currentColor"
                                                  d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/>
                                        </svg>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <hr/>
                </div>
            </main>
        </div>
    )
}

export default Home;