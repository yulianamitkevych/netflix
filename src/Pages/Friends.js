import React from "react";
import SimpleHeader from "../PagesComponents/SimpleHeader";
import LazyLoad from 'react-lazyload';
import '../Styles/Users-list.css';
import '../Styles/Likes.css'
import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import {Redirect, useRouteMatch} from "react-router-dom";

const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);

function Friends() {
    let [UserName, setUserName] = React.useState([]);
    let [UserLastName, setUserLastName] = React.useState([]);
    let [UserAge, setUserAge] = React.useState([]);
    let [UserGender, setUserGender] = React.useState([]);
    let [UserCity, setUserCity] = React.useState([]);
    let [CurrentUser, setCurrentUser] = React.useState([]);
    let [UsersUID, setUsersUID] = React.useState([]);
    let [FriendsList, setFriendsList] = React.useState([]);

    if (!localStorage.getItem('uid')) {
        window.location.replace('/main');
    }

    React.useEffect(() => {
        GetCurrentUserInfo();
        SetUid()
    }, []);

    function SetUid() {
        setUsersUID(UsersUID = localStorage.getItem('uid'));
    }

    function GetCurrentUserInfo() {
        let ref = firebase.database().ref();
        ref.on("value", function (snapshot) {
            let Users = snapshot.val();
            let refPath = 'user_' + localStorage.getItem('uid');
            setCurrentUser(CurrentUser = Users.users[refPath]);
            setUserName(UserName = CurrentUser.name);
            setUserLastName(UserLastName = CurrentUser.last_name);
            setUserAge(UserAge = CurrentUser.age);
            setUserGender(UserGender = CurrentUser.gender);
            setUserCity(UserCity = CurrentUser.city);
            setFriendsList(FriendsList = CurrentUser.friends);
        }, function (error) {
            console.log("Error: " + error.code);
        });
    }

    function RemoveFromFriends(Event) {
        let refPath = 'users/user_' + UsersUID + '/friends';
        let FriendId = Event.target.closest('button').getAttribute('data-user-id');
        let EditFriendsList = FriendsList.filter((Friend) => {
            return Friend.id !== parseInt(FriendId);
        });
        firebase.database().ref(refPath).set(EditFriendsList);
    }

    return (
        <div>
            <SimpleHeader/>
            <main className="main">
                <div className="container">
                    <div className="users-list">
                        <div className="list-heading">
                            <div className={'list__heading-shows'}>Friends</div>
                            <div>|</div>
                            <div className={'list__heading-shows'}><a href={'/shows'}>Shows</a></div>
                            <div>|</div>
                            <div className={'list__heading-users'}><a href={'/people'}>People</a></div>
                        </div>
                        <hr className="user-hr"/>
                        <div className="users">
                            {!FriendsList ? <div>Your Friends list is empty. Return to People and find new
                                friends</div> : FriendsList.map((Friend) => {
                                return <div className="users-user" key={Friend.id}>
                                    <div className="users__user-content">
                                        <div className="users__user-photo">
                                            <LazyLoad className={'users-img-lazy'}>
                                                <img alt={Friend.id} src={Friend?.image?.medium}/>
                                            </LazyLoad>
                                        </div>
                                        <div className="users__user-info">
                                            <div className="users__user__info-name">Name: {Friend?.name}</div>
                                            <div className="users__user__info-date">Date of
                                                birth: {Friend?.birthday}</div>
                                            <div className="users__user__info-sex">Gender: {Friend?.gender}</div>
                                            <div
                                                className="users__user__info-country">Country: {Friend?.country?.name}</div>
                                        </div>
                                    </div>
                                    <div className="users__user-btn">
                                        <button className="add-to-friends" data-user-id={Friend.id}
                                                onClick={RemoveFromFriends}>X
                                        </button>
                                    </div>
                                </div>
                            })}
                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}

export default Friends;