import React from "react";
import '../Styles/Show-list.css';
import '../Styles/Likes.css';
import LazyLoad from 'react-lazyload';
import SimpleHeader from "../PagesComponents/SimpleHeader";

import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import {Redirect, useRouteMatch} from "react-router-dom";

const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);


function LikesPage() {
    let [UserName, setUserName] = React.useState([]);
    let [UserLastName, setUserLastName] = React.useState([]);
    let [UserAge, setUserAge] = React.useState([]);
    let [UserGender, setUserGender] = React.useState([]);
    let [UserCity, setUserCity] = React.useState([]);
    let [CurrentUser, setCurrentUser] = React.useState([]);
    let [UsersUID, setUsersUID] = React.useState([]);
    let [LikesList, setLikesList] = React.useState([]);

    if (!localStorage.getItem('uid')) {
        window.location.replace('/main');
    }

    React.useEffect(() => {
        GetCurrentUserInfo();
        SetUid()
    }, []);

    function SetUid() {
        setUsersUID(UsersUID = localStorage.getItem('uid'));
    }

    function GetCurrentUserInfo() {
        let ref = firebase.database().ref();
        ref.on("value", function (snapshot) {
            let Users = snapshot.val();
            let refPath = 'user_' + localStorage.getItem('uid');
            setCurrentUser(CurrentUser = Users.users[refPath]);
            setUserName(UserName = CurrentUser.name);
            setUserLastName(UserLastName = CurrentUser.last_name);
            setUserAge(UserAge = CurrentUser.age);
            setUserGender(UserGender = CurrentUser.gender);
            setUserCity(UserCity = CurrentUser.city);
            setLikesList(LikesList = CurrentUser.likes);
        }, function (error) {
            console.log("Error: " + error.code);
        });
    }

    function RemoveFromLikes(Event) {
        let refPath = 'users/user_' + UsersUID + '/likes';
        let ShowId = Event.target.closest('div').getAttribute('data-show-id');
        let EditLikeList = LikesList.filter((Show) => {
            return Show.id !== parseInt(ShowId);
        });
        firebase.database().ref(refPath).set(EditLikeList);
    }

    return (
        <div>
            <SimpleHeader/>
            <main className="main show-list-main">
                <div className="container">
                    <div className="shows">
                        <div className="shows-list simple-show-list">
                            <div className="shows-heading">
                                <div className={'shows__heading-follows'}>Likes</div>
                                <div>|</div>
                                <div className={'shows__heading-shows'}><a href={'/shows'}>Shows</a></div>
                                <div>|</div>
                                <div className={'shows__heading-People'}><a href={'/people'}>People</a></div>
                            </div>
                            <hr className="shows-hr"/>
                            <div className="shows-cards">
                                {!LikesList ? <div>Your Likes list is empty. Return to Shows and like
                                    something</div> : LikesList.map((Show) => {
                                    return <div className="shows__cards-card" key={Show.id}>
                                        <div className="shows__cards__card-img">
                                            <a href={`/show/${Show.id}`}>
                                                <LazyLoad>
                                                    <img alt="show-img" src={Show?.image?.medium}/>
                                                </LazyLoad>
                                            </a>
                                        </div>
                                        <div className="shows__cards__card-info">
                                            <div className="shows__cards__card-name">{Show?.name}</div>
                                            <hr className="shows__cards__card-hr"/>
                                            <div className="shows__cards__card-btn">
                                                <div className="shows__cards__card__btn-like" data-show-id={Show.id}
                                                     onClick={RemoveFromLikes}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="far"
                                                         data-icon="heart"
                                                         className="svg-inline--fa fa-heart fa-w-16" role="img"
                                                         xmlns="http://www.w3.org/2000/svg"
                                                         viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                              d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"/>
                                                    </svg>
                                                </div>
                                                <div className="shows__cards__card__btn-rating">
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                         data-icon="star"
                                                         className="svg-inline--fa fa-star fa-w-18" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/>
                                                    </svg>
                                                    <span>{Show?.rating?.average}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}

export default LikesPage;