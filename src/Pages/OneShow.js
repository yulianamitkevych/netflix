import React from "react";
import SimpleHeader from "../PagesComponents/SimpleHeader";
import '../Styles/OneShow.css';
import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import {Redirect, useRouteMatch} from "react-router-dom";
import {
    FacebookShareButton,
    TelegramShareButton,
    TwitterShareButton,
} from "react-share";
import {SocialIcon} from 'react-social-icons';

const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);

function OneShow(props) {
    let [UserName, setUserName] = React.useState([]);
    let [UserLastName, setUserLastName] = React.useState([]);
    let [UserAge, setUserAge] = React.useState([]);
    let [UserGender, setUserGender] = React.useState([]);
    let [UserCity, setUserCity] = React.useState([]);
    let [CurrentUser, setCurrentUser] = React.useState([]);
    let [UsersUID, setUsersUID] = React.useState([]);
    let [ShowHeading, setShowHeading] = React.useState([]);
    let [ShowImg, setShowImg] = React.useState([]);
    let [ShowDescription, setShowDescription] = React.useState([]);
    let [ShowNetwork, setShowNetwork] = React.useState([]);
    let [ShowSchedule, setShowSchedule] = React.useState([]);
    let [ShowStatus, setShowStatus] = React.useState([]);
    let [ShowType, setShowType] = React.useState([]);
    let [ShowGenres, setShowGenres] = React.useState([]);
    let [ShowSite, setShowSite] = React.useState([]);
    let [ShowRating, setShowRating] = React.useState([]);
    let [CurrentShow, setCurrentShow] = React.useState([]);
    let Match = useRouteMatch('/show/:id');
    let SelectedShow = (Match?.params?.id) ? Match?.params?.id : 1;

    if (!localStorage.getItem('uid')) {
        window.location.replace('/main');
    }

    React.useEffect(() => {
        GetCurrentUserInfo();
        GetSelectedShow(SelectedShow);
        SetUid()
    }, []);

    function SetUid() {
        setUsersUID(UsersUID = localStorage.getItem('uid'));
    }

    function GetSelectedShow(ShowId) {
        fetch(`https://api.tvmaze.com/shows/${ShowId}`)
            .then(res => res.json())
            .then((Show) => {
                    let Schedule = `${Show?.schedule?.days.join(', ')} at ${Show?.schedule?.time} (${Show?.runtime} min)`
                    setCurrentShow(CurrentShow = Show);
                    console.log(CurrentShow);
                    setShowHeading(ShowHeading = Show?.name);
                    setShowImg(ShowImg = Show?.image?.medium);
                    setShowDescription(ShowDescription = Show.summary.replace(/(<([^>]+)>)/gi, ""));
                    setShowNetwork(ShowNetwork = Show?.network?.name);
                    setShowSchedule(ShowSchedule = Schedule);
                    setShowStatus(ShowStatus = Show?.status);
                    setShowType(ShowType = Show?.type);
                    setShowGenres(ShowGenres = Show?.genres.join(' | '));
                    setShowSite(ShowSite = Show?.officialSite);
                    setShowRating(ShowRating = Show?.rating?.average);
                },
            );
        window.scrollTo(0, 0);
    }

    function GetCurrentUserInfo() {
        let ref = firebase.database().ref();
        ref.on("value", function (snapshot) {
            let Users = snapshot.val();
            let refPath = 'user_' + localStorage.getItem('uid');
            setCurrentUser(CurrentUser = Users.users[refPath]);
            setUserName(UserName = CurrentUser.name);
            setUserLastName(UserLastName = CurrentUser.last_name);
            setUserAge(UserAge = CurrentUser.age);
            setUserGender(UserGender = CurrentUser.gender);
            setUserCity(UserCity = CurrentUser.city);
        }, function (error) {
            console.log("Error: " + error.code);
        });
    }

    function Like(Event) {
        GetCurrentUserInfo();
        let refPath = 'users/user_' + UsersUID + '/likes';
        if (!CurrentUser?.likes) {
            alert('Success');
            firebase.database().ref(refPath).set([CurrentShow]);
        } else {
            let IsShowLiked = CurrentUser.likes.filter(function (Show) {
                return parseInt(Show.id) === parseInt(CurrentShow.id);
            });

            if (IsShowLiked.length === 0) {
                CurrentUser.likes.push(CurrentShow);
                firebase.database().ref(refPath).set(CurrentUser.likes);
                alert('Success');
            } else {
                alert('You already liked it before');
            }
        }
    }

    function Follow(Event) {
        GetCurrentUserInfo();
        let refPath = 'users/user_' + UsersUID + '/follows';
        if (!CurrentUser?.follows) {
            alert('Success');
            firebase.database().ref(refPath).set([CurrentShow]);
        } else {
            let IsShowFollowed = CurrentUser.follows.filter(function (Show) {
                return parseInt(Show.id) === parseInt(CurrentShow.id);
            });

            if (IsShowFollowed.length === 0) {
                CurrentUser.follows.push(CurrentShow);
                firebase.database().ref(refPath).set(CurrentUser.follows);
                alert('Success');
            } else {
                alert('You already followed it before');
            }
        }
    }

    return (
        <div>
            <SimpleHeader/>
            <main className="main show-list-main">
                <div className="container">
                    <div className={'one-show'}>
                        <div className="shows-heading">
                            <div className={'shows__heading-shows'}><a href={'/shows'}>Shows</a></div>
                            <div>|</div>
                            <div className={'shows__heading-People'}><a href={'/people'}>People</a></div>
                        </div>
                        <hr className="shows-hr"/>
                        <div className={'one__show-name'}>{ShowHeading}</div>
                        <div className={'show-items'}>
                            <div className={'main-items'}>
                                <div className={'show__items-img'}>
                                    <div>
                                        <img src={ShowImg} alt={'img'}/>
                                    </div>
                                    <div className={'show__items-btn'}>
                                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="heart"
                                             className="svg-inline--fa fa-heart fa-w-16" role="img"
                                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" onClick={Like}>
                                            <path fill="currentColor"
                                                  d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"/>
                                        </svg>
                                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="bookmark"
                                             className="svg-inline--fa fa-bookmark fa-w-12" role="img"
                                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" onClick={Follow}>
                                            <path fill="currentColor"
                                                  d="M336 0H48C21.49 0 0 21.49 0 48v464l192-112 192 112V48c0-26.51-21.49-48-48-48zm0 428.43l-144-84-144 84V54a6 6 0 0 1 6-6h276c3.314 0 6 2.683 6 5.996V428.43z"/>
                                        </svg>
                                    </div>
                                </div>
                                <div className={'show__items-description'}>
                                    <div className={'show__items-description-text'}>{ShowDescription}</div>
                                    <div className={'show__items-sharing'}>
                                        <div className={'show__items__sharing-heading'}>Share this on:</div>
                                        <div className={'show__items__sharing-btn'}>
                                            <div className={'sharing'}>
                                                <TwitterShareButton
                                                    url={`https://netflix-595d5.web.app/show/${CurrentShow.id}`}
                                                    title={`Watch ${CurrentShow.name} on MySTREAMING`}
                                                    via={`Watch ${CurrentShow.name} on MySTREAMING`}
                                                    className={"Demo__some-network__share-button"}>
                                                    <SocialIcon
                                                        style={{height: 35, width: 35}}
                                                        type={"twitter"}
                                                        url={`https://twitter.com/intent/tweet?url=https://netflix-595d5.web.app/show/${CurrentShow.id}`}/>
                                                </TwitterShareButton>
                                            </div>
                                            <div className={'sharing'}>
                                                <FacebookShareButton
                                                    url={`https://netflix-595d5.web.app/show/${CurrentShow.id}`}
                                                    title={`https://netflix-595d5.web.app/show/${CurrentShow.id}`}
                                                    className={"Demo__some-network__share-button"}
                                                    quote={`Watch ${CurrentShow.name} on MySTREAMING`}>
                                                    <SocialIcon
                                                        style={{height: 35, width: 35}}
                                                        type={'facebook'}
                                                        url={`https://www.facebook.com/sharer.php?u=https://netflix-595d5.web.app/show/${CurrentShow.id}`}
                                                    />
                                                </FacebookShareButton>
                                            </div>
                                            <div>
                                                <TelegramShareButton
                                                    url={`https://netflix-595d5.web.app/show/${CurrentShow.id}`}
                                                    title={`Watch ${CurrentShow.name} on MySTREAMING`}
                                                    className={"Demo__some-network__share-button"}>
                                                    <SocialIcon
                                                        style={{height: 35, width: 35}}
                                                        type={'telegram'}
                                                        url={`https://telegram.me/share/url?url=https://netflix-595d5.web.app/show/${CurrentShow.id}&text=Watch ${CurrentShow.name} on MySTREAMING`}/>
                                                </TelegramShareButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={'show__items-info'}>
                                <div className={'show__items__info__item-heading'}>Show Info:</div>
                                <div className={'show__items__info-item'}>Network: {ShowNetwork}</div>
                                <div className={'show__items__info-item'}>Schedule: {ShowSchedule}</div>
                                <div className={'show__items__info-item'}>Status: {ShowStatus}</div>
                                <div className={'show__items__info-item'}>Show Type: {ShowType}</div>
                                <div className={'show__items__info-item'}>Genres: {ShowGenres}</div>
                                <div className={'show__items__info-item'}>Official site: <a
                                    href={ShowSite}>{ShowSite}</a></div>
                                <div className={'show__items__info__item-rating'}>
                                    <div>
                                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star"
                                             className="svg-inline--fa fa-star fa-w-18" role="img"
                                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                  d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/>
                                        </svg>
                                    </div>
                                    <div>7.5</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}

export default OneShow;