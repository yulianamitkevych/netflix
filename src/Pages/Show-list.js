import React from "react";
import {Link, Route, useParams, useRouteMatch} from "react-router-dom";
import PropTypes from 'prop-types';
import '../Styles/Show-list.css';
import {logDOM} from "@testing-library/react";
import ReactPaginate from 'react-paginate';
import UserList from "./Users-list";
import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import {Redirect} from "react-router-dom";
import LazyLoad from 'react-lazyload';

const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);


function ShowList(props) {
    let Match = useRouteMatch('/shows/pages/:id');
    let Page = (Match?.params?.id) ? Match?.params?.id : 1;

    let [ShowsList, setShows] = React.useState([]);
    let [GlobalShowList, setGlobalShowList] = React.useState([]);
    let [ShowListForSelects, setShowListForSelects] = React.useState([]);

    let [SearchInputValue, setSearchInputValue] = React.useState([]);
    let [SearchedShowList, setSearchedShowList] = React.useState([]);

    let Status = [];
    let ShowType = [];
    let Genres = [];
    let Languages = [];
    let Country = [];
    let Network = [];
    let WebChannel = [];
    let Rating = ['2+', '3+', '4+', '5+', '6+', '7+', '8+', '9+'];

    let [StatusValue, setStatusValue] = React.useState('');
    let [ShowTypeValue, setShowTypeValue] = React.useState('');
    let [GenreValue, setGenreValue] = React.useState('');
    let [LanguageValue, setLanguageValue] = React.useState('');
    let [CountryValue, setCountryValue] = React.useState('');
    let [NetworkValue, setNetworkValue] = React.useState('');
    let [WebChannelValue, setWebChannelValue] = React.useState('');
    let [RunTimeValue, setRunTimeValue] = React.useState('');
    let [RatingValue, setRatingValue] = React.useState('');

    let [UsersUID, setUsersUID] = React.useState([]);
    let [CurrentUser, setCurrentUser] = React.useState([]);

    let [UserName, setUserName] = React.useState([]);
    let [UserLastName, setUserLastName] = React.useState([]);
    let [UserAge, setUserAge] = React.useState([]);
    let [UserGender, setUserGender] = React.useState([]);
    let [UserCity, setUserCity] = React.useState([]);
    let [UserLikes, setUserLikes] = React.useState([]);

    let FilteredShowList = [];

    if (!localStorage.getItem('uid')) {
        window.location.replace('/main');
    }

    function CleanLocalStorage() {
        localStorage.removeItem('uid');
    }

    function SetUid() {
        setUsersUID(UsersUID = localStorage.getItem('uid'));
    }

    React.useEffect(() => {
        GetPaginationPage(Page);
        GetCurrentUserInfo();
        SetUid();
    }, []);

    function GetCurrentUserInfo() {
        let ref = firebase.database().ref();
        ref.on("value", function (snapshot) {
            let Users = snapshot.val();
            let refPath = 'user_' + localStorage.getItem('uid');
            setCurrentUser(CurrentUser = Users.users[refPath]);
            setUserName(UserName = CurrentUser.name);
            setUserLastName(UserLastName = CurrentUser.last_name);
            setUserAge(UserAge = CurrentUser.age);
            setUserGender(UserGender = CurrentUser.gender);
            setUserCity(UserCity = CurrentUser.city);
        }, function (error) {
            console.log("Error: " + error.code);
        });
    }

    function Like(Event) {
        GetCurrentUserInfo();
        let ShowId = Event.target.closest('button').getAttribute('data-show-id');
        let LikedShows = GetShow(ShowId)[0];
        let refPath = 'users/user_' + UsersUID + '/likes';
        if (!CurrentUser?.likes) {
            alert('Success');
            firebase.database().ref(refPath).set([LikedShows]);
        } else {
            let IsShowLiked = CurrentUser.likes.filter(function (Show) {
                return parseInt(Show.id) === parseInt(LikedShows.id);
            });

            if (IsShowLiked.length === 0) {
                CurrentUser.likes.push(LikedShows);
                firebase.database().ref(refPath).set(CurrentUser.likes);
                alert('Success');
            } else {
                alert('You already liked it before');
            }
        }
    }

    function GetShow(ShowId) {
        return ShowsList.filter(function (Show) {
            return parseInt(Show.id) === parseInt(ShowId);
        })
    }

    function GetPaginationPage(page) {
        fetch(`https://api.tvmaze.com/shows?page=${page}`)
            .then(res => res.json())
            .then((Shows) => {
                    setShows(Shows);
                    setGlobalShowList(Shows);
                    setShowListForSelects(Shows);
                },
            );
        window.scrollTo(0, 0);
    }

    function Pagination() {
        if (ShowsList.length > 200) {
            return (<ul className={'pagination'}>
                <li onClick={() => GetPaginationPage(1)}><a href={`/shows/pages/1`}>1</a></li>
                <li onClick={() => GetPaginationPage(2)}><a href={'/shows/pages/2'}>2</a></li>
                <li onClick={() => GetPaginationPage(3)}><a href={'/shows/pages/3'}>3</a></li>
                <li onClick={() => GetPaginationPage(4)}><a href={'/shows/pages/4'}>4</a></li>
                <li onClick={() => GetPaginationPage(5)}><a href={'/shows/pages/5'}>5</a></li>
            </ul>)
        }
        return (<p/>);
    }

    function Searching() {
        fetch(`https://api.tvmaze.com/search/shows?q=${SearchInputValue}`)
            .then(res => res.json())
            .then((Response) => {
                    let List = [];
                    Response.map((Show) => {
                        List.push(Show.show);
                    });
                    setShows(List);
                },
            );
    }

    function GetSearchInputValue(Event) {
        setSearchInputValue(Event.target.value);
    }

    for (let Show of ShowListForSelects) {
        if (!Status.includes(Show.status)) {
            Status.push(Show.status);
        }
        if (!ShowType.includes(Show.type)) {
            ShowType.push(Show.type);
        }
        Show.genres.forEach(function (Genre) {
            if (!Genres.includes(Genre)) {
                Genres.push(Genre);
            }
        });
        if (!Languages.includes(Show.language)) {
            Languages.push(Show.language);
        }
        if (Show.network?.country?.name !== undefined && !Country.includes(Show.network?.country?.name)) {
            Country.push(Show.network?.country?.name);
        }
        if (Show?.network?.name !== undefined && !Network.includes(Show?.network?.name)) {
            Network.push(Show?.network?.name);
        }
        if (Show?.webChannel?.name !== undefined && !WebChannel.includes(Show?.webChannel?.name)) {
            WebChannel.push(Show?.webChannel?.name);
        }
    }

    function OnchangeStatusSelect(Event) {
        setStatusValue(StatusValue = Event.target.value);
    }

    function OnchangeShowTypeSelect(Event) {
        setShowTypeValue(ShowTypeValue = Event.target.value);
    }

    function OnchangeGenreSelect(Event) {
        setGenreValue(GenreValue = Event.target.value);
    }

    function OnchangeLanguageSelect(Event) {
        setLanguageValue(LanguageValue = Event.target.value);
    }

    function OnchangeCountrySelect(Event) {
        setCountryValue(CountryValue = Event.target.value);
    }

    function OnchangeNetworkSelect(Event) {
        setNetworkValue(NetworkValue = Event.target.value);
    }

    function OnchangeWebChannelSelect(Event) {
        setWebChannelValue(WebChannelValue = Event.target.value);
    }

    function OnchangeRunTimeSelect(Event) {
        setRunTimeValue(RunTimeValue = parseInt(Event.target.value));
    }

    function OnchangeRatingSelect(Event) {
        setRatingValue(RatingValue = Event.target.value);
    }

    function ShowsFilterSelector(Event) {
        Event.preventDefault();
        if (StatusValue ||
            ShowTypeValue ||
            GenreValue ||
            LanguageValue ||
            CountryValue ||
            NetworkValue ||
            WebChannelValue ||
            RunTimeValue ||
            RatingValue) {
            FilteredShowList = GlobalShowList.filter((Show) => {
                return (StatusValue ? Show.status.includes(StatusValue) : true)
                    && (ShowTypeValue ? Show.type.includes(ShowTypeValue) : true)
                    && (GenreValue && Show?.genres ? Show.genres.includes(GenreValue) : true)
                    && (LanguageValue ? Show.language.includes(LanguageValue) : true)
                    && (CountryValue && Show?.network?.country?.name ? Show.network.country.name.includes(CountryValue) : true)
                    && (NetworkValue && Show?.network?.name ? Show.network.name.includes(NetworkValue) : true)
                    && (WebChannelValue && Show?.webChannel?.name ? Show.webChannel.name.includes(WebChannelValue) : true)
                    && ((RunTimeValue && RunTimeValue === 1 && Show?.runtime) ? (Show.runtime < 30) : true)
                    && ((RunTimeValue && RunTimeValue === 2 && Show?.runtime) ? (Show.runtime > 30 && Show.runtime < 60) : true)
                    && ((RunTimeValue && RunTimeValue === 3 && Show?.runtime) ? (Show.runtime > 60) : true)
                    && (RatingValue ? (Show.rating.average > parseInt(RatingValue)) : true)
            });
            setShows(FilteredShowList);
        } else {
            setShows(GlobalShowList);
        }
    }

    return (
        <div>
            <header className="header show-list-header">
                <div className="container header-container">
                    <div className="header-top">
                        <div className="top-icon shows-list-top-icon">MY STREAMING</div>
                        <div className="top-items">
                            <div className="top-search">
                                <label/>
                                <input name="top-search" id="top_search" type="text"
                                       placeholder="Search Shows" onChange={GetSearchInputValue}/>
                                <button className="top__search-btn" onClick={Searching}>
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search"
                                         className="svg-inline--fa fa-search fa-w-16" role="img"
                                         xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 512 512">
                                        <path fill="currentColor"
                                              d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"/>
                                    </svg>
                                </button>
                            </div>
                            <div className="top-user">
                                <div className="user-like">
                                    <a href={'/likes'}>
                                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="heart"
                                             className="svg-inline--fa fa-heart fa-w-16" role="img"
                                             xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                  d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"/>
                                        </svg>
                                    </a>
                                </div>
                                <div className="user-icon dropdown">
                                    <div className="dropdown-content">
                                        <div className="dropdown-user">
                                            <div className="dropdown__user-photo"/>
                                            <div className="dropdown__user-name">{`${UserName} ${UserLastName}`}</div>
                                            <div className={'dropdown__user-info'}>
                                                <div className="dropdown__user-age">Age: {UserAge} year(s)</div>
                                                <div className="dropdown__user-gender">Gender: {UserGender}</div>
                                                <div className="dropdown__user-city">City: {UserCity}</div>
                                            </div>
                                        </div>
                                        <div className="dropdown-personal">
                                            <div className="dropdown__personal-follows">
                                                <a href={'/follows'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="far"
                                                         data-icon="heart"
                                                         className="svg-inline--fa fa-heart fa-w-16" role="img"
                                                         xmlns="http://www.w3.org/2000/svg"
                                                         viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                              d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"/>
                                                    </svg>
                                                    Follows
                                                </a>
                                            </div>
                                            <div className="dropdown__personal-friends">
                                                <a href={'/friends'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                         data-icon="user-plus"
                                                         className="svg-inline--fa fa-user-plus fa-w-20" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                              d="M624 208h-64v-64c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v64h-64c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h64v64c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-64h64c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zm-400 48c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"/>
                                                    </svg>
                                                    Friends
                                                </a>
                                            </div>
                                            <div className="dropdown__personal-friends" onClick={CleanLocalStorage}>
                                                <a href={'/main'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                         data-icon="sign-out-alt"
                                                         className="svg-inline--fa fa-sign-out-alt fa-w-16" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                              d="M497 273L329 441c-15 15-41 4.5-41-17v-96H152c-13.3 0-24-10.7-24-24v-96c0-13.3 10.7-24 24-24h136V88c0-21.4 25.9-32 41-17l168 168c9.3 9.4 9.3 24.6 0 34zM192 436v-40c0-6.6-5.4-12-12-12H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h84c6.6 0 12-5.4 12-12V76c0-6.6-5.4-12-12-12H96c-53 0-96 43-96 96v192c0 53 43 96 96 96h84c6.6 0 12-5.4 12-12z"/>
                                                    </svg>
                                                    Sign out
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main className="main show-list-main">
                <div className="container">
                    <div className="shows">
                        <div className="shows-list">
                            <div className="shows-heading">
                                <div className={'shows__heading-shows'}>Shows</div>
                                <div>|</div>
                                <div className={'shows__heading-People'}><a href={'/people'}>People</a></div>
                            </div>
                            <hr className="shows-hr"/>
                            <div className="shows-cards">
                                {ShowsList.map((Show) => {
                                    let ShowImg = Show?.image;
                                    let ShowRating = Show.rating;

                                    return <div className="shows__cards-card" key={Show.id}>
                                        <div className="shows__cards__card-img">
                                            <a href={`/show/${Show.id}`}>
                                                <LazyLoad>
                                                    <img alt="show-img" id={Show.id} src={ShowImg?.medium}/>
                                                </LazyLoad>
                                            </a>
                                        </div>
                                        <div className="shows__cards__card-info">
                                            <div className="shows__cards__card-name" id={Show.id}>{Show.name}</div>
                                            <hr className="shows__cards__card-hr"/>
                                            <div className="shows__cards__card-btn" id={Show.id}>
                                                <div className="shows__cards__card__btn-like">
                                                    <button onClick={Like} data-show-id={Show.id}>
                                                        <svg aria-hidden="true" focusable="false" data-prefix="far"
                                                             data-icon="heart"
                                                             className="svg-inline--fa fa-heart fa-w-16" role="img"
                                                             xmlns="http://www.w3.org/2000/svg"
                                                             viewBox="0 0 512 512">
                                                            <path fill="currentColor"
                                                                  d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"/>
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div className="shows__cards__card__btn-rating">
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                         data-icon="star"
                                                         className="svg-inline--fa fa-star fa-w-18" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                        <path fill="currentColor"
                                                              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/>
                                                    </svg>
                                                    <span>{ShowRating.average}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                })}
                            </div>
                            <div className={'pagination-list'}>
                                <Pagination/>
                            </div>
                        </div>
                        <div className="filter-group">
                            <form>
                                <label>Show Status</label>
                                <select name="status" onChange={OnchangeStatusSelect}>
                                    <option value=""/>
                                    {Status.map((Value) => {
                                        return <option value={Value} key={Value}>{Value}</option>
                                    })}
                                </select>
                                <label>Show Type</label>
                                <select name="type" onChange={OnchangeShowTypeSelect}>
                                    <option value=""/>
                                    {ShowType.map((Type) => {
                                        return <option value={Type} key={Type}>{Type}</option>
                                    })}
                                </select>
                                <label>Genre</label>
                                <select name="Genre" onChange={OnchangeGenreSelect}>
                                    <option value=""/>
                                    {Genres.map((Genre) => {
                                        return <option value={Genre} key={Genre}>{Genre}</option>
                                    })}
                                </select>
                                <label>Language</label>
                                <select name="Language" onChange={OnchangeLanguageSelect}>
                                    <option value=""/>
                                    {Languages.map((Language) => {
                                        return <option value={Language} key={Language}>{Language}</option>
                                    })}
                                </select>
                                <label>Country</label>
                                <select name="Country" onChange={OnchangeCountrySelect}>
                                    <option value=""/>
                                    {Country.map((Country) => {
                                        return <option value={Country} key={Country}>{Country}</option>
                                    })}
                                </select>
                                <label>Network</label>
                                <select name="Network" onChange={OnchangeNetworkSelect}>
                                    <option value=""/>
                                    {Network.map((NetworkName) => {
                                        return <option value={NetworkName} key={NetworkName}>{NetworkName}</option>
                                    })}
                                </select>
                                <label>Web Channel</label>
                                <select name="web_channel" onChange={OnchangeWebChannelSelect}>
                                    <option value=""/>
                                    {WebChannel.map((Channel) => {
                                        return <option value={Channel} key={Channel}>{Channel}</option>
                                    })}
                                </select>
                                <label>Runtime</label>
                                <select name="Runtime" onChange={OnchangeRunTimeSelect}>
                                    <option value=""/>
                                    <option value={'1'} key={'1'}>{'30 minutes or less'}</option>
                                    <option value={'2'} key={'2'}>{'30 to 60 minutes'}</option>
                                    <option value={'3'} key={'3'}>{'Over 60 minutes'}</option>
                                </select>
                                <label>Rating</label>
                                <select name="Rating" onChange={OnchangeRatingSelect}>
                                    <option value=""/>
                                    {Rating.map((Value) => {
                                        return <option value={Value} key={Value}>{Value}</option>
                                    })}
                                </select>
                            </form>
                            <button className="filter-btn" onClick={ShowsFilterSelector}>Filter</button>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}

ShowList.propTypes = {};

export default ShowList;