import React from "react";
import EnterHeader from "../PagesComponents/EnterHeader";
import '../Styles/Login.css'
import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import {Redirect} from "react-router-dom";

const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);

function Login() {
    let [GetUserLogin, setGetUserLogin] = React.useState([]);
    let [GetUserPassword, setGetUserPassword] = React.useState([]);
    let [ErrorMessage, setErrorMessage] = React.useState([]);
    let [CreatedUserEmail, setCreatedUserEmail] = React.useState([]);
    let [UserRefreshToken, setUserRefreshToken] = React.useState([]);

    if (localStorage.getItem('uid')) {
        window.location.replace('/shows');
    }

    function SignIn(Email, Password) {
        firebase.auth().signInWithEmailAndPassword(Email, Password)
            .then(function (firebaseUser) {
                setCreatedUserEmail(CreatedUserEmail = firebaseUser.user.email);
                setUserRefreshToken(UserRefreshToken = firebaseUser.user.refreshToken)
                localStorage.setItem('email', CreatedUserEmail);
                localStorage.setItem('refreshToken', UserRefreshToken);
                window.location.replace('/shows');
            })
            .catch(function (error) {
                setTimeout(function () {
                    setErrorMessage(ErrorMessage = 'Invalid Email or Password');
                }, 1000);
            });
    }

    function ApplySignIn() {
        SignIn(GetUserLogin, GetUserPassword);
    }

    function GetLogin(Event) {
        setGetUserLogin(Event.target.value);
    }

    function GetPassword(Event) {
        setGetUserPassword(Event.target.value);
    }

    return (
        <div>
            <EnterHeader/>
            <main className="main enter-main">
                <div className="container">
                    <div className="enter-group">
                        <div className="enter-text">Sign in</div>
                        <div className="enter-input">
                            <div className="email-error">{ErrorMessage}</div>
                            <div className="header__main-email enter-email">
                                <label/>
                                <input id="header-email" name="email" placeholder="Email"
                                       type="email" onChange={GetLogin}/>
                            </div>
                            <div className="header__main-email enter-email">
                                <label/>
                                <input id="sign-in-pw" name="password" placeholder="Password"
                                       type="password" onChange={GetPassword}/>
                            </div>
                        </div>
                        <div className="enter-btn enter-btn-login" onClick={ApplySignIn}>Sign in</div>
                    </div>
                </div>
            </main>
        </div>
    )
}

export default Login;