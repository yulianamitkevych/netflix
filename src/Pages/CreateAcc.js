import React from "react";
import EnterHeader from "../PagesComponents/EnterHeader";
import '../Styles/Login.css';
import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';

const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);

function Registration(props) {
    const auth = getAuth();

    let [Name, setName] = React.useState([]);
    let [LastName, setLastName] = React.useState([]);
    let [Gender, setGender] = React.useState([]);
    let [Age, setAge] = React.useState([]);
    let [City, setCity] = React.useState([]);
    let [UserLogin, setUserLogin] = React.useState([]);
    let [Password, setPassword] = React.useState([]);
    let [ErrorMessage, setErrorMessage] = React.useState([]);
    let [UsersUID, setUsersUID] = React.useState([]);
    let [CreatedUserEmail, setCreatedUserEmail] = React.useState([]);
    let [UserRefreshToken, setUserRefreshToken] = React.useState([]);

    if (localStorage.getItem('uid')) {
        window.location.replace('/shows');
    }

    function UserRegistration(Name, LastName, Gender, Age, City, UserLogin, Password) {
        setErrorMessage(ErrorMessage = '');
        firebase.auth().createUserWithEmailAndPassword(UserLogin, Password).then(function (value) {
            setCreatedUserEmail(CreatedUserEmail = value.user.email);
            setUserRefreshToken(UserRefreshToken = value.user.refreshToken);
            localStorage.setItem('uid', firebase.auth().currentUser._delegate.uid);
            CreateUserInDB(Name, LastName, Gender, Age, City, firebase.auth().currentUser._delegate.uid);
            window.location.replace('/shows');
        }).catch(function (value) {
            setTimeout(function () {
                setErrorMessage(ErrorMessage = 'Invalid Email or Password');
            }, 1000);
        });
    }

    function ApplyRegistration() {
        UserRegistration(Name, LastName, Gender, Age, City, UserLogin, Password, UsersUID);
    }

    function CreateUserInDB(Name, LastName, Gender, Age, City, Uid) {
        let refPath = 'users/user_' + Uid;
        firebase.database().ref(refPath).set({
            name: Name,
            last_name: LastName,
            gender: Gender,
            age: Age,
            city: City
        });
    }

    function GetName(Event) {
        setName(Event.target.value);
    }

    function GetLastName(Event) {
        setLastName(Event.target.value);
    }

    function GetGender(Event) {
        setGender(Event.target.value);
    }

    function GetAge(Event) {
        setAge(Event.target.value);
    }

    function GetCity(Event) {
        setCity(Event.target.value);
    }

    function GetLogin(Event) {
        setUserLogin(Event.target.value);
    }

    function GetPassword(Event) {
        setPassword(Event.target.value);
    }

    return (
        <div>
            <EnterHeader/>
            <main className="main enter-main">
                <div className="container">
                    <div className="enter-group">
                        <div className="enter-text">Registration</div>
                        <div className="email-error">{ErrorMessage}</div>
                        <div className="enter-input">
                            <div className="header__main-email enter-email log-input">
                                <label/>
                                <input id="user-name" name="name" placeholder="Name"
                                       type="text" onChange={GetName}/>
                            </div>
                            <div className="header__main-email enter-email log-input">
                                <label/>
                                <input id="user-lastName" name="lastName" placeholder="Last Name"
                                       type="text" onChange={GetLastName}/>
                            </div>
                            <div className="header__main-email enter-email log-input">
                                <div className={'gender-age'}>
                                    <div className="header__main-email enter-email small-input">
                                        <label/>
                                        <input id="gender" name="gender" placeholder="Gender"
                                               type="text" onChange={GetGender}/>
                                    </div>
                                    <div className="header__main-email enter-email small-input">
                                        <label/>
                                        <input id="age" name="age" placeholder="Age"
                                               type="number" onChange={GetAge}/>
                                    </div>
                                </div>
                            </div>
                            <div className="header__main-email enter-email log-input">
                                <label/>
                                <input id="city" name="city" placeholder="City"
                                       type="text" onChange={GetCity}/>
                            </div>
                            <div className="header__main-email enter-email log-input">
                                <label/>
                                <input id="header-email" name="email" placeholder="Email"
                                       type="email" onChange={GetLogin}/>
                            </div>
                            <div className="header__main-email enter-email log-input">
                                <label/>
                                <input id="password" name="password" placeholder="Password"
                                       type="password" onChange={GetPassword}/>
                            </div>
                        </div>
                        <div className="enter-btn registration-btn" onClick={ApplyRegistration}>Submit</div>
                    </div>
                </div>
            </main>
        </div>
    )
}

export default Registration;