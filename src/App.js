import React from "react";
import ReactDOM from 'react-dom';
import {createBrowserHistory} from 'history';
import {Switch, Route, Redirect, Router} from 'react-router-dom';
import {initializeApp} from 'firebase/app';
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import firebase from "firebase/compat";
import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import './App.css';
import Home from "./Pages/Home";
import Login from "./Pages/Login";
import ShowList from "./Pages/Show-list";
import OneShow from "./Pages/OneShow";
import Follows from "./Pages/Follows";
import LikesPage from "./Pages/LikesPage";
import UserList from "./Pages/Users-list";
import Friends from "./Pages/Friends";
import Registration from "./Pages/CreateAcc";
import Footer from "./PagesComponents/Footer";

const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

function App() {
    const history = createBrowserHistory();

    return (
        <div className="App">
            <Router history={history}>
                <Switch>
                    <Route exact path={'/'}>
                        <Redirect to={'/main'}/>
                    </Route>
                    <Route exact path={'/main'} component={Home}/>
                    <Route exact path={'/registration'} component={Registration}/>
                    <Route exact path={'/sign_in'} component={Login}/>
                    <Route exact path={'/shows'} component={ShowList}/>
                    <Route exact path={'/shows/pages/:id'} component={ShowList}/>
                    <Route exact path={'/show/:id'} component={OneShow}/>
                    <Route exact path={'/follows'} component={Follows}/>
                    <Route exact path={'/likes'} component={LikesPage}/>
                    <Route exact path={'/people'} component={UserList}/>
                    <Route exact path={'/friends'} component={Friends}/>
                </Switch>
            </Router>
            <Footer/>
        </div>
    );
}

export default App;
