import React from "react";

function Footer () {
   return (
       <div>
           <footer className="footer">
               <div className="container footer-container">
                   <div className="footer-info">
                       <div className="footer__info-heading">Have questions? Contact us.</div>
                       <div className="footer__info-items">
                           <div className="items-block">
                               <p>Common questions</p>
                               <p>For investors</p>
                               <p>Confidentiality</p>
                               <p>Speed check</p>
                           </div>
                           <div className="items-block">
                               <p>Support Center</p>
                               <p>Vacancies</p>
                               <p>Cookie settings</p>
                               <p>Legal Notices</p>
                           </div>
                           <div className="items-block bottom-item">
                               <p>Account</p>
                               <p>Viewing methods</p>
                               <p>Corporate information</p>
                               <p>Only on MY STREAM</p>
                           </div>
                           <div className="items-block bottom-item">
                               <p>Media center</p>
                               <p>Terms of use</p>
                               <p>Contact us</p>
                           </div>
                       </div>
                   </div>
               </div>
           </footer>
       </div>
   )
}

export default Footer;