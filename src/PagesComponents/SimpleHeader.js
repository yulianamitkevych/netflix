import React from "react";
import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";
import firebase from 'firebase/compat/app';
import 'firebase/auth';
import 'firebase/firestore';
import "firebase/storage";
import {getFirestore, collection, getDocs} from 'firebase/firestore/lite';
import {Redirect, useRouteMatch} from "react-router-dom";

const firebaseConfig = {
    apiKey: "AIzaSyAmMysQzDj6jTRJo_xpFkD-MV5v3bWnrWc",
    authDomain: "netflix-595d5.firebaseapp.com",
    databaseURL: "https://netflix-595d5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "netflix-595d5",
    storageBucket: "netflix-595d5.appspot.com",
    messagingSenderId: "1080135758272",
    appId: "1:1080135758272:web:d3829813d3c4aeca835825"
};

const app = firebase.initializeApp(firebaseConfig);
const db = getFirestore(app);

function SimpleHeader () {
    let [UserName, setUserName] = React.useState([]);
    let [UserLastName, setUserLastName] = React.useState([]);
    let [UserAge, setUserAge] = React.useState([]);
    let [UserGender, setUserGender] = React.useState([]);
    let [UserCity, setUserCity] = React.useState([]);
    let [CurrentUser, setCurrentUser] = React.useState([]);
    let [UsersUID, setUsersUID] = React.useState([]);
    let [LikesList, setLikesList] = React.useState([]);

    function CleanLocalStorage() {
        localStorage.removeItem('uid');
    }

    React.useEffect(() => {
        GetCurrentUserInfo();
        SetUid()
    }, []);

    function SetUid() {
        setUsersUID(UsersUID = localStorage.getItem('uid'));
    }

    function GetCurrentUserInfo() {
        let ref = firebase.database().ref();
        ref.on("value", function (snapshot) {
            let Users = snapshot.val();
            let refPath = 'user_' + localStorage.getItem('uid');
            setCurrentUser(CurrentUser = Users.users[refPath]);
            setUserName(UserName = CurrentUser.name);
            setUserLastName(UserLastName = CurrentUser.last_name);
            setUserAge(UserAge = CurrentUser.age);
            setUserGender(UserGender = CurrentUser.gender);
            setUserCity(UserCity = CurrentUser.city);
            setLikesList(LikesList = CurrentUser.likes);
        }, function (error) {
            console.log("Error: " + error.code);
        });
    }

    return (
        <div>
            <header className="header show-list-header one-show-header">
                <div className="container header-container">
                    <div className="header-top">
                        <div className="top-icon">MY STREAMING</div>
                        <div className="top-items One-Show-Top-items">
                            <div className="top-user">
                                <div className="user-like">
                                    <a href={'/likes'}>
                                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="heart"
                                             className="svg-inline--fa fa-heart fa-w-16" role="img"
                                             xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 0 512 512">
                                            <path fill="currentColor"
                                                  d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"/>
                                        </svg>
                                    </a>
                                </div>
                                <div className="user-icon dropdown">
                                    <div className="dropdown-content">
                                        <div className="dropdown-user">
                                            <div className="dropdown__user-photo"/>
                                            <div className="dropdown__user-name">{`${UserName} ${UserLastName}`}</div>
                                            <div className={'dropdown__user-info'}>
                                                <div className="dropdown__user-age">Age: {UserAge} year(s)</div>
                                                <div className="dropdown__user-gender">Gender: {UserGender}</div>
                                                <div className="dropdown__user-city">City: {UserCity}</div>
                                            </div>
                                        </div>
                                        <div className="dropdown-personal">
                                            <div className="dropdown__personal-follows">
                                                <a href={'/follows'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="far"
                                                         data-icon="bookmark"
                                                         className="svg-inline--fa fa-bookmark fa-w-12" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                                        <path fill="currentColor"
                                                              d="M336 0H48C21.49 0 0 21.49 0 48v464l192-112 192 112V48c0-26.51-21.49-48-48-48zm0 428.43l-144-84-144 84V54a6 6 0 0 1 6-6h276c3.314 0 6 2.683 6 5.996V428.43z"/>
                                                    </svg>
                                                    Follows
                                                </a>
                                            </div>
                                            <div className="dropdown__personal-friends">
                                                <a href={'/friends'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                         data-icon="user-plus"
                                                         className="svg-inline--fa fa-user-plus fa-w-20" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                                        <path fill="currentColor"
                                                              d="M624 208h-64v-64c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v64h-64c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h64v64c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-64h64c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zm-400 48c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"/>
                                                    </svg>
                                                    Friends
                                                </a>
                                            </div>
                                            <div className="dropdown__personal-friends" onClick={CleanLocalStorage}>
                                                <a href={'/main'}>
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                         data-icon="sign-out-alt"
                                                         className="svg-inline--fa fa-sign-out-alt fa-w-16" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                              d="M497 273L329 441c-15 15-41 4.5-41-17v-96H152c-13.3 0-24-10.7-24-24v-96c0-13.3 10.7-24 24-24h136V88c0-21.4 25.9-32 41-17l168 168c9.3 9.4 9.3 24.6 0 34zM192 436v-40c0-6.6-5.4-12-12-12H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h84c6.6 0 12-5.4 12-12V76c0-6.6-5.4-12-12-12H96c-53 0-96 43-96 96v192c0 53 43 96 96 96h84c6.6 0 12-5.4 12-12z"/>
                                                    </svg>
                                                    Sign out
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    )
}

export default SimpleHeader;