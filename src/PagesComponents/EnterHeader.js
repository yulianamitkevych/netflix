import React from "react";

function EnterHeader () {
    return (
        <div>
            <header className='header enter-header' id={'enter-header'}>
                <div className="container header-container">
                    <div className="header-top">
                        <div className="top-icon">MY STREAMING</div>
                    </div>
                </div>
            </header>
        </div>
    )
}

export default EnterHeader;